import firebase from "firebase";

import "firebase/auth";

const firebaseConfig={
    apiKey: "AIzaSyAtq6NfAmQHsqppdxNolA4tU2mKWLSCjIA",
  authDomain: "devcamp-firebase-aae5c.firebaseapp.com",
  projectId: "devcamp-firebase-aae5c",
  storageBucket: "devcamp-firebase-aae5c.appspot.com",
  messagingSenderId: "692857044321",
  appId: "1:692857044321:web:2df619987de6caa9bbd920",
  measurementId: "G-RFE9G1XJZC"
}

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();

