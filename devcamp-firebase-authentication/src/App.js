import logo from './logo.svg';
import './App.css';

import { auth, googleProvider } from "./firebase";
import { useEffect, useState } from "react";

function App() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    auth.signInWithPopup(googleProvider)
      .then((result) => {
        console.log(result);

        setUser(result.user);
      })
      .catch((error) => {
        console.log(error);
      })
  };

  const logoutGoogle = () => {
    auth.signOut()
    .then(()=>{
      setUser(null);
    })
    .catch((error) => {
      console.log(error);
    })
  }

  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      console.log(result);

      setUser(result);
    })
  }, [])



  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
            <>
              <img src={user.photoURL} width={50} height={50} alt="avatar" style={{ borderRadius: "50%" }}></img>
              <p>Hello, {user.displayName}!</p>
              <button onClick={logoutGoogle}>Logout Google</button>
            </>
              :
              <button onClick={loginGoogle}>Login Google</button>
       }

      </header>
    </div>
  );
}

export default App;
